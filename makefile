proposal.pdf: data/announce.tex data/cover.tex data/eval.tex data/report.tex data/survey.tex data/trans.tex proposal.tex data/zjubib.bib
	xelatex proposal.tex
	bibtex proposal
	xelatex proposal.tex
	xelatex proposal.tex
	xelatex proposal.tex

clean:
	rm -fr *.aux *.log *.toc *.nlo data/*.aux *.out *.lof *.bbl *.blg

check:
	grep "Warning" *.log

remove:
	rm proposal.pdf
